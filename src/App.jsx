import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import Books from "./components/Books";
import Form from "./components/Form";
import NavBar from "./components/NavBar";

function App() {
  const [searchValue, setSearchValue] = useState("");

  return (
    <>
      <NavBar onSearchChange={setSearchValue} />
      <Routes>
        <Route path="/" element={<Books searchValue={searchValue} />} />
        <Route path="/register" element={<Form />} />
      </Routes>
    </>
  );
}

export default App;
