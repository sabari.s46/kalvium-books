import React from "react";
import "../App.css";
import KalviumLogo from "../assets/Kal.svg";
import { Link } from "react-router-dom";

const NavBar = ({ onSearchChange }) => {
  const handleSearchChange = (e) => {
    onSearchChange(e.target.value);
  };

  return (
    <nav className="navbar">
      <div className="navbar-container">
        <div className="logo-container">
          <span className="logo-text">
            <Link to="/">
              <img src={KalviumLogo} className="logo" alt="Vite logo" />
            </Link>
          </span>
        </div>
      </div>
      <div className="inputContainer">
        <div className="search-container">
          <input
            className="search-input"
            type="search"
            name="search"
            placeholder="Search"
            onChange={handleSearchChange}
          />
        </div>
      </div>
      <div className="menu-container">
        <div className="link-container">
          <Link to="/register" className="nav-link">
            Register
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
