import React, { useState, useEffect } from "react";
import "../App.css";

export default function Books({ searchValue }) {
  const [books, setBooks] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchBooks = () => {
    return fetch("https://reactnd-books-api.udacity.com/books", {
      headers: { Authorization: "whatever-you-want" },
    }).then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    });
  };

  useEffect(() => {
    fetchBooks()
      .then((data) => {
        setBooks(data.books);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching books:", error.message);
      });
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  } else {
    console.log(books);

    // Filter books based on search value
    const displayedBooks = searchValue
      ? books.filter((book) =>
          book.title.toLowerCase().includes(searchValue.toLowerCase())
        )
      : books;

    return (
      <div className="head">
        <div className="boss">
          {displayedBooks.map((book) => (
            <div key={book.id} className="cards">
              <img
                src={book.imageLinks?.smallThumbnail || "placeholder.jpg"}
                alt=""
              />
              <div className="details">{book.title}</div>
              <div className="details2  ">Free</div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
