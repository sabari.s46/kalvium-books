import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";

export default function Form() {
  const [details, setDetails] = useState(null);
  const [success, setSuccess] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();

  const onSubmit = (data) => {
    // Handle form submission logic here
    setDetails(data);
    setSuccess(true);
  };

  const password = watch("password");

  useEffect(() => {
    // Log details when the state is updated
    console.log(details);
  }, [details]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div id="items">
        {success ? <p className="success">Registration success</p> : <p></p>}
        <div className="heading">Create Account</div>
        <div className="item">
          <input
            type="text"
            {...register("your_name", {
              required: "Please enter your first name",
              minLength: {
                value: 3,
                message: "Name should contain more than 3 characters",
              },
              maxLength: {
                value: 30,
                message: "Name should not contain more than 30 characters",
              },
            })}
            placeholder="Your Name"
          />
          <p className="error">{errors.your_name?.message}</p>
        </div>
        <div className="item">
          <input
            type="text"
            {...register("email", {
              required: "Please enter your email",
              pattern: {
                value: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
                message: "Please enter a valid email address",
              },
            })}
            placeholder="Email"
          />
          <p className="error">{errors.email?.message}</p>
        </div>
        <div className="item">
          <input
            type="password"
            {...register("password", {
              required: "Please enter your password",
              minLength: {
                value: 10,
                message: "Password must be at least 10 characters long",
              },
              validate: (value) => {
                const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(value);
                return (
                  hasSpecialChar || "Password must contain a special character"
                );
              },
            })}
            placeholder="Password"
          />
          <p className="error">{errors.password?.message}</p>
        </div>
        <div className="item">
          <input
            type="password"
            {...register("repassword", {
              required: "Please repeat your password",
              validate: (value) =>
                value === password || "Passwords do not match",
            })}
            placeholder="Repeat Password"
          />
          <p className="error">{errors.repassword?.message}</p>
        </div>
        <div className="item">
          <button type="submit">Sign up</button>
        </div>
      </div>
    </form>
  );
}
